import english from './en';
import deutch from './de';

export default
{
  de: deutch,
  en: english,
};
