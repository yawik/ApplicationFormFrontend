export default
{
  localeName: 'English',
  landingPageTitle: 'Application Forms',
  toolbarTitle: 'Yawik Application Forms',
  howItWorks: {
    title: 'How it works?',
    description: 'You place job advertisements and publish an e-mail address for applications in them? Give us the link to such a job ad. We will then send a mail with a confirmation link to the email address from the job ad. After confirmation we send a 2nd mail with a link to an application form.',
  },
  typewriter: [
    'free?',
    'individual',
    'simple, individual, free!'
  ],
  sendLink:
    {
      title: 'Give us the link to a job advertisement',
      description: 'We try to find an email address for applications in the job advertisement.',
      label: 'Link to Jobad'
    },
  buttons:
    {
      createForm: 'create form',
      sendLink: 'send link',
      back: 'Back',
      send: 'Send',
      cancel: 'Cancel',
      close: 'Close',
      remove: 'Remove',
      preview: 'Demo preview',
    },
  menu:
    {
      form: 'Form',
      tech: 'Technic'
    },
  feature:
    {
      one:
      {
        title: 'No email in the jobad',
        text: 'They no longer need to publish email addresses in the job ad. This way they reduce potential unsolicited mails',
      },
      two:
      {
        title: 'Less callbacks',
        text: 'The form already checks during input whether all required information is available. This reduces the number of inquiries.'
      },
      three:
      {
        title: 'Unified applications',
        text: 'Applications reach their filing more consistently. Try it out.'
      },
      four:
      {
        title: 'Simple introduction',
        text: 'The form is easily integrated into your current workflow. You will receive the application by mail as before. You gain the possibility to control applications via a form.'
      }
    }
};
