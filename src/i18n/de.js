export default
{
  localeName: 'Deutsch',
  landingPageTitle: 'Bewerbungsformulare',
  toolbarTitle: 'Bewerbungsformulare',
  howItWorks: {
    title: 'Wie es funktioniert?',
    description: 'Sie schalten Stellenanzeigen und veröffentlichen darin eine E-Mail Adresse für Bewerbungen? Nennen Sie uns den Link auf eine solche Stellenanzeige. Wir senden dann an die Email Adresse aus der Stellenenzeige eine Mail mit einem Bestätigungslink. Nach Bestätigung senden wir eine 2. Mail mit einem Link zu einem Bewerbungsformular.',
  },
  typewriter: [
    'kostenlos?',
    'individuell',
    'einfach, individuell, kostenlos!'
  ],
  sendLink:
    {
      title: 'Nennen sie uns den Link auf eine Stellenanzeige',
      description: 'Wir versuchen in der Stellenanzeige eine Email Adresse für Bewerbungen zu finden.',
      label: 'Link auf Stellenanzeige'
    },
  buttons:
    {
      createForm: 'Formular erstellen',
      sendLink: 'Link senden',
      back: 'Zurück',
      send: 'Absenden',
      cancel: 'Abbrechen',
      close: 'Schließen',
      remove: 'Löschen',
      preview: 'Demo ansehen',
    },
  menu:
  {
    download: 'Download',
    form: 'Formulare',
    tech: 'Technik'
  },
  feature:
    {
      one:
      {
        title: 'Kein Email in der Anzeige',
        text: 'Sie müssen nicht mehr Email-Adressen in der Stellenanzeige veröffentlichen. Dadurch verringern sie potentiell unerwünschte Mails',
      },
      two:
      {
        title: 'Weniger Rückfragen',
        text: 'Das Formular prüft bereits während der Eingabe, ob alle benötigten Angaben vorhanden sind. Das verringert Rückfragen.'
      },
      three:
      {
        title: 'Einheitliche Bewerbungen',
        text: 'Bewerbungen erreichen ihr Mailpostfach in einer einheitlicheren Art und Weise. Das vereinfacht die Übersicht.'
      },
      four:
      {
        title: 'Einfache Einführung',
        text: 'Das Formular fügt sich in nahtlos in ihren bishgerigen Ablauf ein. Sie erhalten wie zuvor die Bewerbung per Mail. Sie gewinnen sie die Möglichkeit Bewerbungen über ein Formular zu steuern.',
      }
    },
  slides: [
    {
      title: 'Kontakt',
      img: 'screens/screen1-contact.png',
      subtitle: 'Kontaktdaten des Bewerbers',
      text: 'Das formular bietet Eingabefelder für eine Anschrift und für Kontaktdaten wie Telefonnummer oder Email. Das Format von Email Adresse oder Telefonnummern wird bereits während der Eingabe gerüft. Der Bewerber kann auch seine Kontaktdaten aus sozialen Profilen übernehmen'
    },
    {
      title: 'Anschreiben',
      img: 'screens/screen2-coverletter.png',
      subtitle: 'Für das Anschreiben hat der Bewerber einen Editor.',
      text: 'Eingabefeld für das Anschreiben'
    },
    {
      title: 'Anhänge',
      img: 'screens/screen3-attachments.png',
      text: 'Sie bestimmen, welche Daten der Bewerber liefern muss. Rein technisch kann ein Bewerber mehrere Dokumente einfach per Drag and Drop in der Bewerbung ablegen. Auch das Hinzufügen eines Profilbild ist möglich. ',
      subtitle: 'Sie bestimmen, welche Daten Sie benötigen'
    },
    {
      title: 'Voransicht',
      img: 'screens/screen4-preview.png',
      text: 'Der Bewerber kann jederzeit seine Daten in einer Voransicht betrachten. Er kann jederzeit seine Daten wieder löschen.',
      subtitle: 'Voransicht alle erfassten Daten'
    },
    {
      title: 'Datenschutz',
      img: 'screens/screen5-privacy.png',
      test: 'Der Benutzer kann seine Daten im Formular zusammenstellen. Das Formular versendet keine Cookies.',
      subtitle: ''
    }]
};
