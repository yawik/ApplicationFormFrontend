# Application Form Landingpage

Based on the Quasar Framework, this is a starter page for creating a landing page. Includes two custom components - Timer and Typewriter. I may add more over time.

Forked from https://github.com/meditatingdragon/quasar-landing-page-starter

## Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn run lint
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).


